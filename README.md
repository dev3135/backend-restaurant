# Backend restaurant

## Environnement de production (HTTP uniquement)

Connexion interface gestionnaire

[Quai Antique Backend PROD](http://quai-antique-alex.herokuapp.com/)

L'application frontend se trouve à l'adresse suivante :

[Quai Antique Frontend PROD](http://quai-antique-front.herokuapp.com/)


## Prérequis Déploiement en local

Pour déployer la partie frontend en local suivre les étapes : 

- [ ] [Déploiement en local du frontend](https://gitlab.com/dev3135/frontend-restaurant)

Un serveur apache et une base de données MYSQL
XAMPP 8.2.0.0 
 
- [ ] [Symfony ClI Version > 5.4.21](https://symfony.com/download)

## Récupérer le projet

```
git clone git@gitlab.com:dev3135/backend-restaurant.git
cd backend-restaurant
```

Copier le fichier .env.local.exemple en .env.local

```
cp .env.local.exemple .env.local
```

## Installer le Runtime symfony

```
composer require symfony/runtime
```

## Démarrer le serveur Symfony

```
symfony server:start
```

## Créer la base de données

```
php bin/console doctrine:database:create
```

## Créer les tables

```
php bin/console doctrine:schema:update --force
```

## Ajouter des données

```
php bin/console doctrine:fixtures:load
```

## Tests de l'application

- [ ] [Connexion gestionnaire](http://127.0.0.1:8000/login)

Les noms d'utilisateurs et mots de passe sont
déjà crées via les fixtures.

```
admin@restaurant.com
password
```

## Documentation API 

```
https://documenter.getpostman.com/view/25720023/2s93JzMgXU
```

## Librairies utilisées

```
composer require security
composer require lexik/jwt-authentication-bundle
composer require symfony/maker-bundle --dev
composer require orm-fixtures --dev
composer require sensio/framework-extra-bundle
composer require easycorp/easyadmin-bundle
composer require vich/uploader-bundle
composer require nelmio/cors-bundle
```
