<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Categorie;
use App\Entity\Formule;
use App\Entity\Jour;
use App\Entity\Menu;
use App\Entity\Plat;
use App\Entity\Reservation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Date;
use DateTime;
use Symfony\Component\Validator\Constraints\Time;

class AppFixtures extends Fixture
{
    private $userPasswordHasher;
    
    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }


    public function load(ObjectManager $manager): void
    {

        //créer une categorie 
       
        $categorieBurger = new Categorie();
        $categorieBurger->setTitre("Burger");
        $manager->persist($categorieBurger);
        
        $categorieDessert = new Categorie();
        $categorieDessert->setTitre("Dessert");
        $manager->persist($categorieDessert);
       
        $categorieEntree = new Categorie();
        $categorieEntree->setTitre("Entrée");
        $manager->persist($categorieEntree);

        $categoriePlat = new Categorie();
        $categoriePlat->setTitre("Plat");
        $manager->persist($categoriePlat);

        $formuleMidiMer = new Formule();
        $formuleMidiMer->setTitre("Formule du midi Mer");
        $formuleMidiMer->setDescription("Entrée + Plat ou Plat + Dessert");
        $formuleMidiMer->setPrix(16.90);
        $manager->persist($formuleMidiMer);

        $formuleSoirMer = new Formule();
        $formuleSoirMer->setTitre("Formule du Soir Mer");
        $formuleSoirMer->setDescription("Entrée + Plat + Dessert");
        $formuleSoirMer->setPrix(25.90);
        $manager->persist($formuleSoirMer);

        $formuleMidiMontagnard = new Formule();
        $formuleMidiMontagnard->setTitre("Formule du midi Montagnard");
        $formuleMidiMontagnard->setDescription("Entrée + Plat ou Plat + Dessert");
        $formuleMidiMontagnard->setPrix(14.90);
        $manager->persist($formuleMidiMontagnard);

        $formuleSoirMontagnard = new Formule();
        $formuleSoirMontagnard->setTitre("Formule du Soir Montagnard");
        $formuleSoirMontagnard->setDescription("Entrée + Plat + Dessert");
        $formuleSoirMontagnard->setPrix(22.90);
        $manager->persist($formuleSoirMontagnard);

        // Création menus avec des formules
        $menuMer = new Menu();
        $menuMer->setTitre("Menu de la Mer");
        $menuMer->addFormule($formuleMidiMer);
        $menuMer->addFormule($formuleSoirMer);
        $manager->persist($menuMer);

        $menuMontagnard = new Menu();
        $menuMontagnard->setTitre("Menu montagnard");
        $menuMontagnard->addFormule($formuleMidiMontagnard);
        $menuMontagnard->addFormule($formuleSoirMontagnard);
        $manager->persist($menuMontagnard);

        // creation des plats 
        
        $plat = new Plat();
        $plat->setTitre("Lazagne à la bolognaise");
        $plat->setDescription('description');
        $plat->setCategorie($categoriePlat);
        $plat->setPrix(12.90);
        $manager->persist($plat);

        $plat = new Plat();
        $plat->setTitre("Moule frite");
        $plat->setDescription('description');
        $plat->setCategorie($categoriePlat);
        $plat->setPrix(12.90);
        $plat->setMenu($menuMer);
        $manager->persist($plat);

        $plat = new Plat();
        $plat->setTitre("Burger montagnard");
        $plat->setDescription('description');
        $plat->setPrix(14.90);
        $plat->setCategorie($categorieBurger);
        $plat->setMenu($menuMontagnard);
        $manager->persist($plat);

        $plat = new Plat();
        $plat->setTitre("Crumble au pomme");
        $plat->setDescription('description');
        $plat->setPrix(7.90);
        $plat->setCategorie($categorieDessert);
        $plat->setMenu($menuMontagnard);
        $manager->persist($plat);

        $plat = new Plat();
        $plat->setTitre("Créme brûler");
        $plat->setDescription('description');
        $plat->setPrix(6.90);
        $plat->setCategorie($categorieDessert);
        $plat->setMenu($menuMer);
        $manager->persist($plat);

        $plat = new Plat();
        $plat->setTitre("Terrine de poisson");
        $plat->setDescription('description');
        $plat->setPrix(10.90);
        $plat->setCategorie($categorieEntree);
        $plat->setMenu($menuMer);
        $manager->persist($plat);

        $plat = new Plat();
        $plat->setTitre("Assiete de charcuterie");
        $plat->setDescription('description');
        $plat->setPrix(10.90);
        $plat->setCategorie($categorieEntree);
        $plat->setMenu($menuMontagnard);
        $manager->persist($plat);

        $reservation = new Reservation();
        $reservation->setNom("Visiteur 1");
        $reservation->setNbCouvert(10);
        $reservation->setDate(new DateTime('2023-03-10'));
        $reservation->setHeure(new \DateTime('12:00'));
        $reservation->setAllergie(false);
        $manager->persist($reservation);

        $reservation = new Reservation();
        $reservation->setNom("Visiteur 2");
        $reservation->setNbCouvert(10);
        $reservation->setDate(new DateTime('2023-03-10'));
        $reservation->setHeure(new \DateTime('12:00'));
        $reservation->setAllergie(true);
        $manager->persist($reservation);

        $reservation = new Reservation();
        $reservation->setNom("Visiteur 3");
        $reservation->setNbCouvert(12);
        $reservation->setDate(new DateTime('2023-03-10'));
        $reservation->setHeure(new \DateTime('12:15'));
        $reservation->setAllergie(true);
        $manager->persist($reservation);

        $user = new User();
        $user->setEmail("alexandra@restaurant.com");
        $user->setNom("Salvador");
        $user->setPrenom("Alexandra");
        $user->setRoles(["ROLE_CLIENT"]);
        $user->setAllergie(true);
        $user->setNbCouvert(5);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, "alexandra"));
        $manager->persist($user);    
    
        $user = new User();
        $user->setEmail("guillaume@restaurant.com");
        $user->setNom("BRION");
        $user->setPrenom("Guillaume");
        $user->setRoles(["ROLE_CLIENT"]);
        $user->setAllergie(false);
        $user->setNbCouvert(2);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, "guillaume"));
        $manager->persist($user);
        
        $user = new User();
        $user->setEmail("matheo@restaurant.com");
        $user->setNom("Salvador");
        $user->setPrenom("Mathéo");
        $user->setRoles(["ROLE_CLIENT"]);
        $user->setAllergie(false);
        $user->setNbCouvert(15);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, "matheo"));
        $manager->persist($user);

        // Création d'un user admin
        $userAdmin = new User();
        $userAdmin->setEmail("admin@restaurant.com");
        $userAdmin->setNom("Administrateur");
        $userAdmin->setPrenom("Administrateur");
        $userAdmin->setRoles(["ROLE_ADMIN"]);
        $userAdmin->setNbCouvert(0);
        $userAdmin->setAllergie(false);
        $userAdmin->setPassword($this->userPasswordHasher->hashPassword($userAdmin, "password"));
        $manager->persist($userAdmin);

        //Création des jours 
        $lundi = new Jour();
        $lundi->setTitre("lundi");
        $manager->persist($lundi);

        $mardi = new Jour();
        $mardi->setTitre("mardi");
        $manager->persist($mardi);

        $mercredi = new Jour();
        $mercredi->setTitre("mercredi");
        $manager->persist($mercredi);

        $jeudi = new Jour();
        $jeudi->setTitre("jeudi");
        $manager->persist($jeudi);

        $vendredi = new Jour();
        $vendredi->setTitre("vendredi");
        $manager->persist($vendredi);

        $samedi = new Jour();
        $samedi->setTitre("samedi");
        $manager->persist($samedi);

        $dimanche = new Jour();
        $dimanche->setTitre("dimanche");
        $manager->persist($dimanche);

        $manager->flush();
    }

}
