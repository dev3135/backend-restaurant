<?php

namespace App\Controller;
use App\Entity\Formule;
use App\Repository\FormuleRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormuleController extends AbstractController
{
   
    #[Route('api/formules', name:"formules", methods:["GET"])]
    public function getformuleList(FormuleRepository $formuleRepository, serializerInterface $serializer): JsonResponse
    {
        $formuleList = $formuleRepository->findAll();
        $jsonFormuleList = $serializer->serialize($formuleList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonFormuleList, Response::HTTP_OK, [], true);
    }

    #[Route('api/formules/{id}', name: 'formule', methods: ['GET'])]
    public function getformuleById(Formule $formule, SerializerInterface $serializer): JsonResponse 
    {
        $jsonFormule = $serializer->serialize($formule, 'json',['groups' => 'getPlat']);
        return new JsonResponse($jsonFormule, Response::HTTP_OK, ['accept' => 'json'], true);
    }

}
