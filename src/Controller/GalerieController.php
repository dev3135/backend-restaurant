<?php

namespace App\Controller;

use App\Entity\Galerie;
use App\Repository\GalerieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class GalerieController extends AbstractController
{
    #[Route('/api/galerie', name: 'Galerie', methods: ['GET'])]
    public function getGalerie(Request $request, GalerieRepository $galerieRepository, serializerInterface $serializer): JsonResponse
    {
        $path = $request->server->get('HTTP_HOST')."/uploads/galerie/";
        $galerieList = $galerieRepository->findAll();
        /** @var Galerie $galerie */
        foreach($galerieList as $galerie){
            if(!empty($galerie->getImage())){
                $galerie->setImage("http://".$path.$galerie->getImage());
            }
        }
        $jsonGalerieList = $serializer->serialize($galerieList, 'json');
        return new JsonResponse($jsonGalerieList, Response::HTTP_OK, [], true);
    }
}
