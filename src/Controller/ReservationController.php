<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface as GeneratorUrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;


class ReservationController extends AbstractController
{
    #[Route('api/reservations', name: 'reservations', methods:["GET"])]
    public function getReservationList(ReservationRepository $reservationRepository, serializerInterface $serializer): JsonResponse
    {
        $reservationList = $reservationRepository->findAll();
        $jsonreservationList = $serializer->serialize($reservationList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonreservationList, Response::HTTP_OK, [], true);
    }

    #[Route('api/reservations/{id}', name: 'reservation', methods: ['GET'])]
    public function getReservationById(Reservation $reservation, SerializerInterface $serializer): JsonResponse 
    {
        $jsonreservationList= $serializer->serialize($reservation, 'json',['groups' => 'getPlat']);
        return new JsonResponse($jsonreservationList, Response::HTTP_OK, ['accept' => 'json'], true);
    }

    #[Route('api/reservations/{id}', name: 'deleteReservation', methods: ['DELETE'])]
    public function deleteReservation(Reservation $reservation, EntityManagerInterface $em): JsonResponse 
    {
        $em->remove($reservation);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/api/reservations', name:"createReservation", methods: ['POST'])]
    public function createReservation(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, GeneratorUrlGeneratorInterface $urlGenerator): JsonResponse 
    {
        $reservation= $serializer->deserialize($request->getContent(), Reservation::class, 'json');
        $em->persist($reservation);
        $em->flush();

        $jsonReservation = $serializer->serialize($reservation, 'json', ['groups' => 'getPlat']);
        
        $reservation = $urlGenerator->generate('plats', ['id' => $reservation->getId()], GeneratorUrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonReservation, Response::HTTP_CREATED, ["plat" => $reservation], true);
    }
 
    #[Route('api/reservations/{id}', name:"updateReservations", methods:['PUT'])]
    public function updateReservation(Request $request, SerializerInterface $serializer, Reservation $currentReservation, EntityManagerInterface $em): JsonResponse 
    {
        $updatedReservation = $serializer->deserialize($request->getContent(), 
                Reservation::class, 
                'json', 
                [AbstractNormalizer::OBJECT_TO_POPULATE => $currentReservation]);
        $em->persist($updatedReservation);
        $em->flush();
        
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    #[Route('api/reservations/couverts/{filtres}', name:"Reservations couverts", methods:['GET'])]
    public function getReservationsCouverts(Request $request, SerializerInterface $serializer, ReservationRepository $reservationRepository, EntityManagerInterface $em): JsonResponse 
    {
        $heure = $request->query->get('heure');
        $date = $request->query->get('date');
        $filtres = [
            'date' => $date,
            'heure' => $heure,
        ];
        $result = $reservationRepository->findReservationsCouvertsByDate($filtres);
        $result = end($result);
        $json = $serializer->serialize($result, 'json',['groups'=> 'getRecettes']);
       
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

}




