<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Repository\MenuRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    #[Route('api/menus', name:"Menus", methods:["GET"])]
    public function getMenuList(MenuRepository $MenuRepository, serializerInterface $serializer): JsonResponse
    {
        $menuList = $MenuRepository->findAll();
        $jsonMenuList = $serializer->serialize($menuList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonMenuList, Response::HTTP_OK, [], true);
    }

    #[Route('api/menus/{id}', name: 'menu', methods: ['GET'])]
    public function getMenuById(Menu $menu, SerializerInterface $serializer): JsonResponse 
    {
        $jsonMenu = $serializer->serialize($menu, 'json',['groups' => 'getPlat']);
        return new JsonResponse($jsonMenu, Response::HTTP_OK, ['accept' => 'json'], true);
    }

}