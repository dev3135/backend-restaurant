<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): RedirectResponse
    {
        // Rediriger l'utilisateur vers la page d'administration EasyAdmin
        return $this->redirectToRoute('admin');
    }
}
