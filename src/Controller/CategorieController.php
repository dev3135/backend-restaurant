<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    #[Route('api/categories', name:"Categories", methods:["GET"])]
    public function getCategoriesList(CategorieRepository $categorieRepository, serializerInterface $serializer): JsonResponse
    {
        $categorieList = $categorieRepository->findAll();
        $jsonCategorieList = $serializer->serialize($categorieList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonCategorieList, Response::HTTP_OK, [], true);
    }

    #[Route('api/categories/{id}', name: 'Categorie', methods: ['GET'])]
    public function getCategoriesById(Categorie $categorie, SerializerInterface $serializer): JsonResponse 
    {
        $jsonCategorie = $serializer->serialize($categorie, 'json',['groups' => 'getPlat']);
        return new JsonResponse($jsonCategorie, Response::HTTP_OK, ['accept' => 'json'], true);
    }

}