<?php

namespace App\Controller;

use App\Repository\BoutiqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BoutiqueController extends AbstractController
{
    #[Route('api/seuil', name:"Seuil reservation", methods:["GET"])]
    public function getSeuil(BoutiqueRepository $boutiqueRepository, SerializerInterface $serializer): JsonResponse 
    {
        $boutiqueList = $boutiqueRepository->findAll();
        
        $seuilConvive = end($boutiqueList);

        $jsonBoutique = $serializer->serialize($seuilConvive, 'json');
       
        return new JsonResponse($jsonBoutique, Response::HTTP_OK, [], true);
    }
}
