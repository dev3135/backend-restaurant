<?php

namespace App\Controller;

use App\Entity\Jour;
use App\Repository\JourRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JourController extends AbstractController
{
    #[Route('api/jours', name:"jours", methods:["GET"])]
    public function getJourList(JourRepository $jourRepository, serializerInterface $serializer): JsonResponse
    {
        $jourList = $jourRepository->findAll();
        $jsonJourList = $serializer->serialize($jourList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonJourList, Response::HTTP_OK, [], true);
    }

    #[Route('api/jours/{id}', name: 'jour', methods: ['GET'])]
    public function getJourById(Jour $jour, SerializerInterface $serializer): JsonResponse 
    {
        $jsonJour= $serializer->serialize($jour, 'json',['groups' => 'getPlat']);
        return new JsonResponse($jsonJour, Response::HTTP_OK, ['accept' => 'json'], true);
    }

}