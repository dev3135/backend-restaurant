<?php

namespace App\Controller;

use App\Entity\Plat;
use App\Repository\PlatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface as GeneratorUrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class PlatController extends AbstractController
{
    #[Route('api/plats', name:"plats", methods:["GET"])]
    public function getplatList(PLatRepository $platRepository, serializerInterface $serializer): JsonResponse
    {
        $platList = $platRepository->findAll();
        $jsonPlatList = $serializer->serialize($platList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonPlatList, Response::HTTP_OK, [], true);
    }

    #[Route('api/plats/{id}', name: 'plat', methods: ['GET'])]
    public function getPlatById(Plat $plat, SerializerInterface $serializer): JsonResponse 
    {
        $jsonPlat = $serializer->serialize($plat, 'json',['groups'=> 'getPlat']);
        return new JsonResponse($jsonPlat , Response::HTTP_OK, ['accept' => 'json'], true);
    }
    
    #[Route('api/plats/{id}', name: 'delete plat', methods: ['DELETE'])]
    public function deletePlat(Plat $plat, EntityManagerInterface $em): JsonResponse 
    {
        $em->remove($plat);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/api/plats', name:"createplats", methods: ['POST'])]
    public function createPlat(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, GeneratorUrlGeneratorInterface $urlGenerator): JsonResponse 
    {
        $plat = $serializer->deserialize($request->getContent(), Plat::class, 'json');
        $em->persist($plat);
        $em->flush();

        $jsonPlat = $serializer->serialize($plat, 'json', ['groups' => 'getPlat']);
        
        $plat = $urlGenerator->generate('plats', ['id' => $plat->getId()], GeneratorUrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonPlat, Response::HTTP_CREATED, ["plat" => $plat], true);
   }

 
    #[Route('api/plats/{id}', name:"updatePlats", methods:['PUT'])]
    public function updatePlat(Request $request, SerializerInterface $serializer, Plat $currentPlat, EntityManagerInterface $em): JsonResponse 
    {
        $updatedPlat = $serializer->deserialize($request->getContent(), 
                Plat::class, 
                'json', 
                [AbstractNormalizer::OBJECT_TO_POPULATE => $currentPlat]);
        $em->persist($updatedPlat);
        $em->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}


