<?php

namespace App\Controller\Admin;

use App\Entity\Horaire;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class HoraireCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Horaire::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle(Crud::PAGE_INDEX, "Liste des horaires");
        $crud->setPageTitle(Crud::PAGE_NEW, "Créer un horaire");
        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('jour'),
            TimeField::new('heureDebut', 'Heure de début')->setFormat('HH:mm'),
            TimeField::new('heureFin', 'Heure de fin')->setFormat('HH:mm'),
            BooleanField::new('isMidi', 'Horaire du midi'),
        ];
    }
    
}
