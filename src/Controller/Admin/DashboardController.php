<?php

namespace App\Controller\Admin;

use App\Entity\Boutique;
use App\Entity\Formule;
use App\Entity\Menu;
use App\Entity\Plat;
use App\Entity\Reservation;
use App\Entity\Categorie;
use App\Entity\Galerie;
use App\Entity\Horaire;
use App\Entity\Jour;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator) {

    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $url = $this->adminUrlGenerator->setcontroller(PlatCrudController::class)
        ->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Gestion restaurant');
    }

    public function configureMenuItems(): iterable
    {
        // Todo ajouter les permissions 
        yield MenuItem::section('Gestion boutique');
        yield MenuItem::subMenu('Gérer les horaires', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud("Ajouter un horaire",'fas fa-plus', Horaire::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Voir les horaires ",'fas fa-eyes', Horaire::class),
            MenuItem::linkToCrud("Ajouter un jour",'fas fa-plus', Jour::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Voir les jours ",'fas fa-eyes', Jour::class)
        ]); 
        yield MenuItem::subMenu('Gérer seuil', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud("Seuil de réservation",'fas fa-plus', Boutique::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Modifier seuil ",'fas fa-eyes', Boutique::class)
        ]); 
         yield MenuItem::section('Gestion des menus');
         yield MenuItem::subMenu('Menus', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud("Créer un menu ",'fas fa-plus', Menu::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Voir les menus ",'fas fa-eyes', Menu::class)
         ]);
         yield MenuItem::section('Gestion des formules');
         yield MenuItem::subMenu('Formule', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud("Ajouter une formule",'fas fa-plus', Formule::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Voir les formules ",'fas fa-eyes', formule::class)
        ]);
        yield MenuItem::section('Gestion des plats');
        yield MenuItem::subMenu('Plat', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud("Créer un plat",'fas fa-plus', Plat::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Voir les plats ",'fas fa-eyes', Plat::class)
        ]);
        yield MenuItem::subMenu('Categorie', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud("Ajouter une categorie",'fas fa-plus', Categorie::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud("Voir les categories ",'fas fa-eyes', Categorie::class)
         ]);
         yield MenuItem::section('Gestion des reservations');
         yield MenuItem::subMenu('Reservation', 'fas fa-bars')->setSubItems([
             MenuItem::linkToCrud("Ajouter une reservation",'fas fa-plus', Reservation::class)->setAction(Crud::PAGE_NEW),
             MenuItem::linkToCrud("Voir les reservations ",'fas fa-eyes', Reservation::class)
         ]);
         yield MenuItem::section('Galerie');
         yield MenuItem::subMenu('Photos', 'fas fa-bars')->setSubItems([
             MenuItem::linkToCrud("Ajouter des photos",'fas fa-plus', Galerie::class)->setAction(Crud::PAGE_NEW),
             MenuItem::linkToCrud("Voir la galerie ",'fas fa-eyes', Galerie::class)
         ]);
    }

}