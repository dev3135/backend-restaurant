<?php

namespace App\Controller\Admin;

use App\Entity\Reservation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ReservationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Reservation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle(Crud::PAGE_INDEX, "Planing des reservations");
        $crud->setPageTitle(Crud::PAGE_NEW, "Créer une nouvelle reservation");
        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            BooleanField::new('allergie'),
            NumberField::new('nbCouvert'),
            DateTimeField::new('date'),
        ];
    }
    
}
