<?php

namespace App\Controller;

use App\Entity\Horaire;
use App\Repository\HoraireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HoraireController extends AbstractController
{
    #[Route('api/horaires', name:"horaires", methods:["GET"])]
    public function getHoraireList(HoraireRepository $horaireRepository, serializerInterface $serializer): JsonResponse
    {
        $horaireList = $horaireRepository->findAll();
        $jsonHoraireList = $serializer->serialize($horaireList, 'json',['groups'=> 'getPlat']);
       
        return new JsonResponse($jsonHoraireList, Response::HTTP_OK, [], true);
    }

    #[Route('api/horaires/{id}', name: 'horaire', methods: ['GET'])]
    public function getHoraireById(Horaire $horaire, SerializerInterface $serializer): JsonResponse 
    {
        $jsonHoraire = $serializer->serialize($horaire, 'json',['groups' => 'getPlat']);
        return new JsonResponse($jsonHoraire, Response::HTTP_OK, ['accept' => 'json'], true);
    }
}
