<?php

namespace App\Entity;

use App\Repository\BoutiqueRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BoutiqueRepository::class)]
class Boutique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $seuilConvive = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeuilConvive(): ?int
    {
        return $this->seuilConvive;
    }

    public function setSeuilConvive(int $seuilConvive): self
    {
        $this->seuilConvive = $seuilConvive;

        return $this;
    }
}
